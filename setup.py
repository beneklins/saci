from setuptools import setup, find_packages
import saci

setup(
    name='Saci',
    version=str(saci.__VERSION__),
    packages=find_packages(),
    description='arXiv search results crawler routine',
    long_description=str('Saci is a routine to query arXiv for pre-prints'
                         'of interest and extract their information.'),
    # url='https://...',
    license="GPLv3",
    install_requires=['six', 'termcolor', 'ansi2html'],
    entry_points={
        'console_scripts': ['saci=saci.saci:main'],
    }
)

